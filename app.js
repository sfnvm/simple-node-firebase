const express = require('express');
const cors = require('cors');

const app = express();

// Request Body Parser
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Allow Cross Origin
app.use(cors({ origin: true }));

const {
  initializeApp,
  applicationDefault,
  cert,
} = require('firebase-admin/app');
const {
  getFirestore,
  Timestamp,
  FieldValue,
} = require('firebase-admin/firestore');

// User credential
const serviceAccount = require('./serviceAccountKey.json');

// Initialize Firebase
initializeApp({
  credential: cert(serviceAccount),
});
const db = getFirestore();

// Func to interact with Firecloud
async function createUsersRecord(user) {
  await db.collection('users').add(user);
}

// Utilities functions
function validateEmail(email) {
  const superFancyFunctionFromTheInternet =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return superFancyFunctionFromTheInternet.test(String(email).toLowerCase());
}

// Routes
app
  .route('/login')
  .get((req, res) => {
    console.log(req.headers);
    res.send('Please use POST method');
  })
  .post((req, res) => {
    // Validate empty/blank 'username'
    if (!req.body.username) {
      res.status(400);
      res.send({
        error: 'Username must not empty',
      });
      return;
    }

    // Validate empty/blank 'password'
    if (!req.body.password) {
      res.status(400);
      res.send({
        error: 'Password must not empty',
      });
      return;
    }

    // Validate empty/blank 'email'
    if (!req.body.email) {
      res.status(400);
      res.send({
        error: 'Email must not empty',
      });
      return;
    }

    // Validate Email
    if (!validateEmail(req.body.email)) {
      res.status(400);
      res.send({
        error: 'Email format invalid',
      });
      return;
    }

    userToInsert = {
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
      createdDate: Timestamp.now(),
    };

    createUsersRecord(userToInsert);
    res.status(201).send(userToInsert);
  });

const port = 3000;

app.listen(port, console.log(`Server is listening on port ${port}`));
